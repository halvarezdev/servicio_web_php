<?php

  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
  header("Access-Control-Allow-Headers: Content-Type, Authorization, Accept, X-Requested-With, x-xsrf-token");
  header("Content-Type: application/json; charset=utf-8");

  include "library/config.php";
  
  $postjson = json_decode(file_get_contents('php://input'), true);
  //$today    = date('Y-m-d');



  switch ($postjson['send']) {


    case 'registrar':
      
      
      $query = mysqli_query($mysqli, "INSERT INTO app_registro_delivery SET
        app_name = '$postjson[app_name]',
        app_direccion= '$postjson[app_direccion]',
        app_celular = '$postjson[app_celular]',
        app_wsp = '$postjson[app_wsp]',
        app_maps = '$postjson[app_maps]',
        app_ganancia_vendedor = '$postjson[app_ganancia_vendedor]',
        app_ganancia_total = '$postjson[app_ganancia_total]',
        app_numero_pedido = '$postjson[app_numero_pedido]',
        app_vendedor_id = '$postjson[app_vendedor_id]',
        app_fecha = '$postjson[app_fecha]',
        app_estado   = '1'
      ");
 
      if($query) $result = json_encode(array('success'=>true, 'msg'=>'Registrado exitosamente!'));
      else $result = json_encode(array('success'=>false, 'msg'=>'Error, intente nuevamente'));
    
      echo $result;

    break;

    
    case 'reporte':
      
      
      $data = array();
      $query = mysqli_query($mysqli, "SELECT * FROM app_registro_delivery 
      WHERE app_vendedor_id='$postjson[app_vendedor_id]' 
      and date(app_fecha) between CURDATE() AND CURDATE() 
      ORDER BY app_fecha DESC LIMIT $postjson[start],$postjson[limit]");
  
      while($row = mysqli_fetch_array($query)){
  
        $data[] = array(
  
         
          'app_name' => $row['app_name'],
          'app_direccion'=> $row['app_direccion'],
          'app_celular' => $row['app_celular'],
          'app_wsp' => $row['app_wsp'],
          'app_maps' => $row['app_maps'],
          'app_ganancia_vendedor' => $row['app_ganancia_vendedor'],
          'app_ganancia_total' => $row['app_ganancia_total'],
          'app_numero_pedido' => $row['app_numero_pedido'],
          'app_fecha' => $row['app_fecha'],
    
        );
      }
  
      if($query) $result = json_encode(array('success'=>true, 'result'=>$data));
      else $result = json_encode(array('success'=>false));
  
      echo $result;

    break;

    case 'kpis':
      
      
      $data_total = array();
      $query = mysqli_query($mysqli, "SELECT SUM(app_ganancia_vendedor) AS app_ganancia_hoy FROM app_registro_delivery 
      WHERE app_vendedor_id='$postjson[app_vendedor_id]' and date(app_fecha) between CURDATE() AND CURDATE() ORDER BY app_fecha DESC LIMIT 50");
 
  
      while($row = mysqli_fetch_array($query)){
  
        $data_total[] = array(
  
         
          'app_ganancia_hoy' => $row['app_ganancia_hoy'],
      
    
        );
      }
  
      if($query) $result = json_encode(array('success'=>true, 'total'=>$data_total));
      else $result = json_encode(array('success'=>false));
  
      echo $result;

    break;
   
   
    default:


      $result = json_encode(array('success'=>false, 'msg'=>'¡ERROR!'));

     echo $result;
    
    break;
  }

?>