<?php

  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
  header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
  header("Content-Type: application/json; charset=utf-8");

  include "library/config.php";
  
  $postjson = json_decode(file_get_contents('php://input'), true);
  $today    = date('Y-m-d');

//Añadiendo nombre y descripcion de la tabla master_customer
  if($postjson['aksi']=='add'){

  	$query = mysqli_query($mysqli, "INSERT INTO master_customer 
      SET
  		name_customer = '$postjson[name_customer]',
  		desc_customer = '$postjson[desc_customer]',
  		created_at	  = '$today' 
  	");

  	$idcust = mysqli_insert_id($mysqli);

  	if($query) $result = json_encode(array('success'=>true, 'customerid'=>$idcust));
  	else $result = json_encode(array('success'=>false, 'msg'=>'Cuenta desactivada'));

  	echo $result;

  }//fin else 
 //-----------------------------
//'getdata' de la tabla master_customer
  elseif($postjson['aksi']=='getdata'){
  	$data = array();
  	$query = mysqli_query($mysqli, "SELECT * FROM master_customer ORDER BY customer_id DESC LIMIT $postjson[start],$postjson[limit]");

  	while($row = mysqli_fetch_array($query)){

  		$data[] = array(
  			'customer_id' => $row['customer_id'],
  			'name_customer' => $row['name_customer'],
  			'desc_customer' => $row['desc_customer'],
  			'created_at' => $row['created_at'],

  		);
  	}

  	if($query) $result = json_encode(array('success'=>true, 'result'=>$data));
  	else $result = json_encode(array('success'=>false));

  	echo $result;

  }//fin 'getdata'
//----------------------------
  //update master_customer
  elseif($postjson['aksi']=='update'){
  	$query = mysqli_query($mysqli, "UPDATE master_customer SET 
  		name_customer='$postjson[name_customer]',
  		desc_customer='$postjson[desc_customer]' WHERE customer_id='$postjson[customer_id]'");

  	if($query) $result = json_encode(array('success'=>true, 'result'=>'success'));
  	else $result = json_encode(array('success'=>false, 'result'=>'error'));

  	echo $result;

  }//fin update
  //-------------
//delete customer 
  elseif($postjson['aksi']=='delete'){
  	$query = mysqli_query($mysqli, "DELETE FROM master_customer WHERE customer_id='$postjson[customer_id]'");

  	if($query) $result = json_encode(array('success'=>true, 'result'=>'success'));
  	else $result = json_encode(array('success'=>false, 'result'=>'error'));

  	echo $result;

  }//fin delete

//----------------
  //proces 'login'
  elseif($postjson['aksi']=='login'){
    $password = md5($postjson['password']);
    $query = mysqli_query($mysqli, "SELECT * FROM master_user WHERE username='$postjson[username]' AND password='$password'");
    $check = mysqli_num_rows($query);

    if($check>0){
      $data = mysqli_fetch_array($query);
      $datauser = array(
        'user_id' => $data['user_id'],
        'username' => $data['username'],
        'password' => $data['password']
      );

      if($data['status']=='y'){
        $result = json_encode(array('success'=>true, 'result'=>$datauser));
       }else {
         $result = json_encode(array('success'=>false, 'msg'=>'Cuenta desactivada')); 
      }

    }else{
      $result = json_encode(array('success'=>false, 'msg'=>'Usuario o Password incorrecto'));
    }

    echo $result;
  }//fin proces 'login'

  //----------------------

  //proceso 'register'

  elseif($postjson['aksi']=='register'){
    $password = md5($postjson['password']);
    $query = mysqli_query($mysqli, "INSERT INTO master_user SET
      username = '$postjson[username]',
      password = '$password',
      status   = 'y'
    ");

    if($query) $result = json_encode(array('success'=>true));
    else $result = json_encode(array('success'=>false, 'msg'=>'Error, intente nuevamente'));

    echo $result;
  }// fin 'register'
  //------------------------------
  // proceso recovery password

 elseif($postjson['aksi']=='recovery'){
    $password = md5($postjson['password']);
    $query = mysqli_query($mysqli, "UPDATE master_user SET
      password = '$password' WHERE username='$postjson[username]'");

    if($query) $result = json_encode(array('success'=>true));
    else $result = json_encode(array('success'=>false, 'msg'=>'username not exist'));

    echo $result;
  }// fin recovery

  //marcar asistencia

  elseif($postjson['aksi']=='make_time'){

    $query = mysqli_query($mysqli, "INSERT INTO master_time 
      SET
      fecha_user = '$postjson[fecha_user]',
      hora_user = '$postjson[hora_user]',
      username = '$postjson[username]'
      
    ");

    $idcust = mysqli_insert_id($mysqli);

    if($query) $result = json_encode(array('success'=>true, 'customerid'=>$idcust));
    else $result = json_encode(array('success'=>false));

    echo $result;

  }// fin asistencia
  //-------------
  //ver reporte

  elseif($postjson['aksi']=='report'){
    $data = array();
    $query = mysqli_query($mysqli, "SELECT * FROM master_time WHERE username='$postjson[username]' ORDER BY id_time DESC LIMIT $postjson[start],$postjson[limit]");

    while($row = mysqli_fetch_array($query)){

      $data[] = array(

        'id_time' => $row['id_time'],
        'username' => $row['username'],
        'fecha_user' => $row['fecha_user'],
        'hora_user' => $row['hora_user'],

      );
    }

    if($query) $result = json_encode(array('success'=>true, 'result'=>$data));
    else $result = json_encode(array('success'=>false));

    echo $result;

  }// fin ver reporte






?>